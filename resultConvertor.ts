type rarity = "Godly" | "Mythic" | "Legendary" | "Rare" | "Uncommon" | "Common"
interface MainCard {
    name: string,
    card_type: "MainCard",
    rarity: rarity
    image_url: string,
    rizz_number: number,
    health: number
    attack: number,
    effect: string,
    id: string
}

interface RizzCard {
    name: string,
    card_type: "RizzCard",
    rizz_number: number,
    image_url: string,
    id: string
}

interface AbilityCard {
    name: string,
    card_type: "AbilityCard",
    rarity: rarity
    image_url: string,
    ability: string,
    id: string
}
const result: (MainCard | RizzCard | AbilityCard)[] = [
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 10, name: "Rizzler 10", id: "rizzler_10"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 9, name: "Rizzler 9" , id: "rizzler_9"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 8, name: "Rizzler 8", id: "rizzler_8"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 7, name: "Rizzler 7", id: "rizzler_7"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 6, name: "Rizzler 6", id: "rizzler_6"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 5, name: "Rizzler 5", id: "rizzler_5"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 4, name: "Rizzler 4", id: "rizzler_4"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 3, name: "Rizzler 3", id: "rizzler_3"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 2, name: "Rizzler 2", id: "rizzler_2"},
    { card_type: "RizzCard", image_url: "https://ewan.cephas.monster/ewan_goofy/20230731-221718-Ewanlike-2023.png", rizz_number: 1, name: "Rizzler 1", id: "rizzler_1"},
    {
        name: "Ewan",
        card_type: "MainCard",
        rarity: "Godly",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1274347654.jpg",
        rizz_number: 10,
        health: 1000000,
        attack: 1000000,
        effect: "Triple your health and attack",
        id: "ewan"
    },
    {
        name: "Smasher",
        card_type: "MainCard",
        rarity: "Mythic",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231211-170824-Snapchat.jpg",
        rizz_number: 7,
        health: 50,
        attack: 10000,
        effect: "Smashing the opponent",
        id: "smasher"
    },
    {
        name: "Flipper <Zero>",
        card_type: "MainCard",
        rarity: "Mythic",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231211-170742-Snapchat.jpg",
        rizz_number: 7,
        health: 500,
        attack: 500,
        effect: "flip others health and attack",
        id: "flipperzero"
    },
    {
        name: "Healthy Ewan",
        card_type: "MainCard",
        rarity: "Legendary",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231211-170735-Snapchat.jpg",
        rizz_number: 6,
        health: 250000,
        attack: 250,
        effect: "Just have a lot of health",
        id: "healthy_ewan"
    },
    {
        name: "Muscular Ewan",
        card_type: "MainCard",
        rarity: "Legendary",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231209_121954_1707E196.png",
        rizz_number: 6,
        health: 250,
        attack: 250000,
        effect: "I am so Strong",
        id: "muscular_ewan"
    },
    {
        name: "Peaky Ewan",
        card_type: "MainCard",
        rarity: "Legendary",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-858736857.jpg",
        rizz_number: 6,
        health: 250,
        attack: 250,
        effect: "divide others health",
        id: "peaky_ewan"
    },
    {
        name: "Cheaky Ewan",
        card_type: "MainCard",
        rarity: "Rare",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1071603262.jpg",
        rizz_number: 3,
        health: 200,
        attack: 200,
        effect: "-100 others attack",
        id: "cheaky_ewan"
    },
    {
        name: "Ewan Cheating",
        card_type: "MainCard",
        rarity: "Rare",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-700635579.jpg",
        rizz_number: 3,
        health: 200,
        attack: 200,
        effect: "Look at people's card",
        id: "ewan_cheating"
    },
    {
        name: "Ewan Shocker 1st",
        card_type: "MainCard",
        rarity: "Uncommon",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-2133915432.jpg",
        rizz_number: 2,
        health: 20,
        attack: 200,
        effect: "Literially nothing",
        id: "ewan_shocker_1st"
    },
    {
        name: "Ewan Shocker 2rd",
        card_type: "MainCard",
        rarity: "Uncommon",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231211-170738-Snapchat.jpg",
        rizz_number: 2,
        health: 200,
        attack: 20,
        effect: "Literially nothing",
        id: "ewan_shocker_2rd"
    },
    {
        name: "Basic",
        card_type: "MainCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231209_121953_FE5BA879.png",
        rizz_number: 1,
        health: 150,
        attack: 150,
        effect: "You have Ewan-ed",
        id: "basic"
    },
    {
        name: "Ewan?",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20230328-191956-Image.png",
        ability: "Add 100 on your own health",
        id: "ewan_question_mark"
    },
    {
        name: "Ewan!",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231211-170742-Snapchat.jpg",
        ability: "Add 100 on your own attack",
        id: "ewan_exclamation_mark"
    },
    {
        name: "Uncommon Health",
        card_type: "AbilityCard",
        rarity: "Uncommon",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231209_121953_4C4D2BC2.png",
        ability: "Add 200 on your own health",
        id: "uncommon_health"
    },
    {
        name: "Uncommon Attacker",
        card_type: "AbilityCard",
        rarity: "Uncommon",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231211-170729-Snapchat.jpg",
        ability: "Add 200 on your own attack",
        id: "uncommon_attacker"
    },
    {
        name: "Noob Berserker",
        card_type: "AbilityCard",
        rarity: "Rare",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231211-170851-Snapchat.jpg",
        ability: "Add 300 on your own attack",
        id: "noob_berserker"
    },
    {
        name: "Berserker",
        card_type: "AbilityCard",
        rarity: "Legendary",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231209_121953_FE5BA879.png",
        ability: "Add 500 on your own attack",
        id: "berserker"
    },
    {
        name: "Mad Berserker",
        card_type: "AbilityCard",
        rarity: "Mythic",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231209_121953_FE5BA879.png",
        ability: "Add 1000 on your own attack",
        id: "mad_berserker"
    },
    {
        name: "God Berserker",
        card_type: "AbilityCard",
        rarity: "Godly",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "Add 10000 on your own attack",
        id: "god_berserker"
    },
    {
        name: "God Smasher",
        card_type: "AbilityCard",
        rarity: "Godly",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "x4 on your own attack",
        id: "god_smasher"
    },
    {
        name: "Smasher",
        card_type: "AbilityCard",
        rarity: "Mythic",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "x2 on your own attack",
        id: "smasher"
    },
    {
        name: "Lava",
        card_type: "AbilityCard",
        rarity: "Mythic",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "Literially remove one card from other people",
        id: "lava"
    },
    {
        name: "Strong Shield",
        card_type: "AbilityCard",
        rarity: "Mythic",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "+1000 on health",
        id: "strong_shield"
    },
    {
        name: "Shield",
        card_type: "AbilityCard",
        rarity: "Rare",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "+100 on health",
        id: "shield"
    },
    {
        name: "Baby Shield",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "+50 on health",
        id: "baby_shield"
    },
    {
        name: "Weak Shield",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "+10 on health",
        id: "weak_shield"
    },
    {
        name: "Extra Shirt",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/Snapchat-1779955675.jpg",
        ability: "+50 on health",
        id: "extra_shirt"
    },
    {
        name: "Extra Underwear",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20230328_192130_646FB1AD.jpg",
        ability: "+30 on health",
        id: "extra_underwear"
    },
    {
        name: "Leaky Defence",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231209_121955_607483EC.png",
        ability: "-30 on health",
        id: "leaky_defence"
    },
    {
        name: "Leaky Attack",
        card_type: "AbilityCard",
        rarity: "Common",
        image_url: "https://ewan.cephas.monster/ewan_goofy/20231209_121955_607483EC.png",
        ability: "-30 on health",
        id: "leaky_attack"
    },
]
Deno.writeTextFile("result.json", JSON.stringify(result))
// const cmd = new Deno.Command("python", {args: ["main.py"]})
// const { code, stdout, stderr } = cmd.outputSync()
// // console.log(new TextDecoder().decode(code))
// console.log(new TextDecoder().decode(stdout))
// console.log(new TextDecoder().decode(stderr))
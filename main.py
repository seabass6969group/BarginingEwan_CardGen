# https://raw.githubusercontent.com/github/gitignore/main/Python.gitignore

from PIL import Image, ImageDraw, ImageFont
import math
import json
import requests
import multiprocessing
# import urllib.request 

font = ImageFont.truetype("./static/InriaSans-Regular.ttf", 35* 4)
font2 = ImageFont.truetype("./static/InriaSans-Regular.ttf", 45 *4)
RARITY = ["Godly", "Mythic" ,"Legendary", "Rare", "Uncommon", "Common"]
def genMainCard(rare: str, number: int, center_image: str, health: int, attack: int, effect: str, save: str, name: str):
    rarity_image = Image.open(r"./static/rarity.png")
    Rizz_number = Image.open(r"./static/Rizz_number.png")
    background = Image.open(r"./static/background.png")
    bottom = Image.open(r"./static/Bottom.png")
    LoveIcon = Image.open("./static/Love.png")
    KatanaIcon = Image.open("./static/Katana.png")
    if rare not in RARITY:
        print("Stop")
        return 
    if number <= 0 or number >= 11:
        print("Stop")
        return 
    RIZZ_NUMBER_WIDTH = 93 * 4
    RIZZ_NUMBER_HEIGHT = 92 * 4
    RIZZ_LEFT_POS = 497 * 4
    RIZZ_TOP_POS = 15 * 4
    RARITY_NUMBER_WIDTH = 600 * 4
    RARITY_NUMBER_HEIGHT = 143 * 4
    main_stage_image = Image.open(center_image)
    # scaleFactor_main_stage = background.width /main_stage_image.width  
    scaleFactor_main_stage = background.width / main_stage_image.width
    main_stage = main_stage_image.resize((
        math.floor(background.width), 
        math.floor(scaleFactor_main_stage * main_stage_image.height)
    ), resample=Image.Resampling.BILINEAR)
    newRarity_image = rarity_image.crop((
        RARITY_NUMBER_WIDTH * RARITY.index(rare), 
        0, 
        RARITY_NUMBER_WIDTH * RARITY.index(rare) + RARITY_NUMBER_WIDTH, 
        RARITY_NUMBER_HEIGHT
    ))
    newRizz_number = Rizz_number.crop((
        RIZZ_NUMBER_WIDTH * (10 - number), 
        0, 
        RIZZ_NUMBER_WIDTH * (10 - number) + RIZZ_NUMBER_WIDTH, 
        RIZZ_NUMBER_HEIGHT
    ))

    newRarity_image.paste(newRizz_number, (RIZZ_LEFT_POS, RIZZ_TOP_POS), newRizz_number)
    
    background.paste(main_stage, (0, 106*4))
    background.paste(bottom, (0-10, 531*4), bottom)
    background.paste(newRarity_image, (0,0), newRarity_image)
    draw = ImageDraw.Draw(background)
    draw.text((42 * 4, 424 * 4 + 143 * 4 + 100), "Effect: {}".format(effect), font=font, fill=(0,0,0))
    background.paste(LoveIcon, (42*4, 424 * 4 + 143 * 4 + 100*3), LoveIcon)
    background.paste(KatanaIcon, (42*4, 424 * 4 + 143 * 4 + 550), KatanaIcon)
    draw.text((153 * 4, 533 * 4 + 45), "{}".format(name), font=font)
    draw.text((42 * 9, 424 * 4 + 143 * 4 + 100*3), "Health: {}".format(health), font=font2, fill=(0,0,0))
    draw.text((42 * 9, 424 * 4 + 143 * 4 + 100*5.5), "Attack: {}".format(attack), font=font2, fill=(0,0,0))
    background.save("./output/main/{}.png".format(save))
    # background.show()

def genRizzCard(number: int, center_image: str, save: str, name: str):
    Rizz_number = Image.open(r"./static/Rizz_number.png")
    background = Image.open(r"./static/background.png")
    bottom = Image.open(r"./static/Bottom_rizzcard.png")
    seperator = Image.open(r"./static/seperator.png")
    if number <= 0 or number >= 11:
        print("Stop")
        return 
    RIZZ_NUMBER_WIDTH = 93 * 4
    RIZZ_NUMBER_HEIGHT = 92 * 4
    RIZZ_LEFT_POS = 497 * 4
    RIZZ_TOP_POS = 15 * 4
    main_stage_image = Image.open(center_image)
    # scaleFactor_main_stage = background.width /main_stage_image.width  
    scaleFactor_main_stage = background.width / main_stage_image.width
    main_stage = main_stage_image.resize((
        math.floor(background.width), 
        math.floor(scaleFactor_main_stage * main_stage_image.height)
    ), resample=Image.Resampling.BILINEAR)

    newRizz_number = Rizz_number.crop((
        RIZZ_NUMBER_WIDTH * (10 - number), 
        0, 
        RIZZ_NUMBER_WIDTH * (10 - number) + RIZZ_NUMBER_WIDTH, 
        RIZZ_NUMBER_HEIGHT
    ))

    background.paste(main_stage, (0, 0))
    background.paste(bottom, (0-10, math.floor(71.5*4)), bottom)
    background.paste(seperator, (43 * 4, 218 * 4), seperator)
    background.paste(newRizz_number, (253 * 4 ,110* 4), newRizz_number)
    background.save("./output/rizz/{}.png".format(save))
    # background.show()

def splitTo(string: str, limiter = 37) -> [str]:
    lines = []
    result = []
    splited = string.split()
    charlength = 0
    last_index = 0
    for index,item in enumerate(splited):
        # print(index, len(item), item)
        charlength += len(item) + 1
        if charlength >= limiter:
            lines.append(splited[last_index:index])
            last_index = index + 1
            charlength = 0
    lines.append(splited[last_index:])
    for i in lines:
        text = ""
        for x in i:
            text += x + " "
        result.append(text)
    return result
def genAbilityCard(rare: str, center_image: str, ability: str, save: str, name: str):
    rarity_image = Image.open(r"./static/rarity.png")
    Rizz_number = Image.open(r"./static/Rizz_number.png")
    background = Image.open(r"./static/background.png")
    bottom = Image.open(r"./static/Bottom.png")
    LoveIcon = Image.open("./static/Love.png")
    KatanaIcon = Image.open("./static/Katana.png")
    if rare not in RARITY:
        print("Stop")
        return 
    RARITY_NUMBER_WIDTH = 600 * 4
    RARITY_NUMBER_HEIGHT = 143 * 4
    main_stage_image = Image.open(center_image)
    # scaleFactor_main_stage = background.width /main_stage_image.width  
    scaleFactor_main_stage = background.width / main_stage_image.width
    main_stage = main_stage_image.resize((
        math.floor(background.width), 
        math.floor(scaleFactor_main_stage * main_stage_image.height)
    ), resample=Image.Resampling.BILINEAR)
    newRarity_image = rarity_image.crop((
        RARITY_NUMBER_WIDTH * RARITY.index(rare), 
        0, 
        RARITY_NUMBER_WIDTH * RARITY.index(rare) + RARITY_NUMBER_WIDTH, 
        RARITY_NUMBER_HEIGHT
    ))

    
    background.paste(main_stage, (0, 106*4))
    background.paste(bottom, (0-10, 531*4), bottom)
    background.paste(newRarity_image, (0,0), newRarity_image)
    draw = ImageDraw.Draw(background)
    draw.text((42 * 4, 424 * 4 + 143 * 4 + 100), "Ability: ", font=font, fill=(0,0,0))
    draw.text((153 * 4, 533 * 4 + 45), "{}".format(name), font=font)
    for index, i in enumerate(splitTo(ability)):
        draw.text((42 * 4, 424 * 4 + 143 * 4 + 100*(3 + 1.7 * index)), "{}".format(i), font=font, fill=(0,0,0))
    background.save("./output/ability/{}.png".format(save))
# genMainCard("Godly", 5, r"./ewan.png", 100,100,"No effect")
# genRizzCard(10, "./ewan.png")
# genAbilityCard("Godly", "./ewan.png", "Nothing really specially at the end of the day. This card haha")
# print(splitTo("Nothing really at the end of the day. This card is not the best card in the world"))

def runner(item, index):
    temp_image = "temp/{}_tmp.jpg".format(index)
    # url = 
    data = requests.get(item['image_url']).content 
    f = open(temp_image,'wb') 
    f.write(data) 
    f.close() 
    if item['card_type'] == "MainCard":
        genMainCard(item['rarity'], item['rizz_number'], temp_image, item['health'], item['attack'], item['effect'], item['id'], item['name'])
    if item['card_type'] == "RizzCard":
        genRizzCard(item['rizz_number'], temp_image, item['id'], item['name'])
    if item['card_type'] == "AbilityCard":
        genAbilityCard(item['rarity'], temp_image, item['ability'] ,item['id'], item['name'])

    print(index)
jsonRead = open("result.json", "r").read()
for index, item in enumerate(json.loads(jsonRead)):
    runner(item, index)

